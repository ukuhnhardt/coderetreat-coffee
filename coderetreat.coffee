# Note: All code run below is running in a closure.
#
# This means that to make a variable global, you must assign it to `window`.
#
# See http://jashkenas.github.com/coffee-script/#scripts for more info

Cell = (x, y, c) ->
  x : x
  y : y
  c : c

Cell = (x, y) ->
  x : x
  y : y
  c : '#f0f'

#spaceship
# population = [Cell(1,0), Cell(2,0), Cell(3,0), Cell(3, -1), Cell(2, -2)];
#F
#population = [Cell(1,0), Cell(2,0), Cell(0,1), Cell(1, 1), Cell(1, 2)];
# diehard
population = [Cell(7,0), Cell(1,1), Cell(2,1), Cell(2, 2),Cell(6, 2),Cell(7, 2),Cell(8, 2) ];
newPopulation = [];
clickPopulation = [];

iteration = 0;
intervalKeeper = 0;

Array::merge = (other) -> Array::push.apply @, other
Array::remove = (e) -> @[t..t] = [] if inCellArray

paintCell = (cell, ctx) ->
  ctx.fillStyle = cell.c;
  ctx.fillRect cell.x*5 + 300 , cell.y*5 + 300 , 5, 5 ;

clearCell = (cell, ctx) ->
  ctx.fillStyle = '#fff';
  ctx.fillRect cell.x*5 + 300 , cell.y*5 + 300 , 5, 5 ;

inCellArray = (cell, cells) ->
  for c in cells
    if c.x == cell.x and c.y == cell.y
      return true;
  return false;

getCellNeighbors = (cell) ->
  x = cell.x;
  y = cell.y;
  c = cell.c
  return [Cell(x-1,y-1,c), Cell(x, y-1), Cell(x+1,y-1,c),
                     Cell(x-1,y),                 Cell(x+1,y),
                     Cell(x-1,y+1,c), Cell(x, y+1), Cell(x+1,y+1,c) ];

countNeighbors = (cell, completePopulation) ->
  cellNeighbors = getCellNeighbors( cell );
  neighbors = ( n for n in cellNeighbors when not inCellArray n, completePopulation )
#  console.log(8-neighbors.length + " neighbors for " + cell.x + "," + cell.y );
  return neighbors;

propagateNeighbors = (cells, completePopulation, newPopulation) ->
  neighbors = countNeighbors( cells[0], completePopulation);
  n = 8 - neighbors.length;
  if n == 3 and not inCellArray cells[0], newPopulation
    newPopulation[newPopulation.length] = cells[0];
#    console.log("cell " + cells[0].x + "," + cells[0].y + " born")

  if cells.length > 1
    propagateNeighbors( cells.slice(1), completePopulation, newPopulation );


updatePopulation = (cells, completePopulation, newPopulation) ->
  neighbors = countNeighbors( cells[0], completePopulation);
  n = 8 - neighbors.length;
  if n == 2 or n == 3 and not inCellArray( cells[0], newPopulation )
    newPopulation[newPopulation.length] = cells[0];
#    console.log("cell " + cells[0].x + "," + cells[0].y + " survived")

  if neighbors.length > 0
    propagateNeighbors( neighbors, completePopulation, newPopulation )

  if cells.length > 1
    updatePopulation( cells.slice(1), completePopulation, newPopulation)

doGame = ( canvas, ctx )->
  newPopulation = [];
#  console.log('doGame pop ' + population.length + ' new pop ' + newPopulation.length );
  copyOfClickPop = clickPopulation.slice()
  population.merge  copyOfClickPop
  clickPopulation = []
  if population.length is 0
    clearInterval intervalKeeper;
    $('#gameoflife').html('<h1>R.I.P</h1>');
    return;
  try
    cells = updatePopulation( population, population, newPopulation)
  catch error
    alert(error);
  canvas.width = canvas.width;
#  console.log('doGame after update pop ' + population.length + ' new pop ' + newPopulation.length );
  paintCell(cell, ctx) for cell in newPopulation;
  population = newPopulation.slice();
  $('#it').html( iteration++ );
#  alert('done')

$ ->
  canvas = window.document.getElementById('golCanvas');

  canvas.addEventListener 'click',
    (evt) ->
          mousePos = window.getMousePos(canvas, evt);
          newX = parseInt (mousePos.x - 300)/5;
          newY = parseInt (mousePos.y - 300)/5;
          if inCellArray(Cell(newX, newY), clickPopulation )
            clickPopulation.remove( Cell(newX, newY) )
            clearCell(Cell(newX, newY), ctx)
          else
            clickPopulation[clickPopulation.length] = Cell(newX, newY, '#00f')
            paintCell(cell, ctx) for cell in clickPopulation;
            console.log("added cell to clickpopulation (" + clickPopulation.length + ") " + newX + ":" + newY)
    , false

  ctx=canvas.getContext('2d');
  ctx.canvas.width  = window.innerWidth;
  ctx.canvas.height = window.innerHeight;
#  ctx.fillStyle='#FF0000';
  paintCell(cell, ctx) for cell in population;

  $('#startstop').bind 'click', (evt) ->
      if $(this).text() is 'start'
        doGame( canvas, ctx );
        intervalKeeper = setInterval ( -> doGame( canvas, ctx ) ), 1000;
        $(this).text('stop')
      else if $(this).text() is 'stop'
        clearInterval intervalKeeper;
        $(this).text('start')

###
  doGame( canvas, ctx );
  alert('ok');
  doGame( canvas, ctx );
  alert('ok');
  doGame( canvas, ctx );
  alert('ok');
  doGame( canvas, ctx );
###



